pub enum Size {
    Bytes,
    KiloBytes,
    MegaBytes,
    GigaBytes
}