use std::fs;
use std::io::Error;
use std::path::Path;

use clap::ArgMatches;

use super::config::Config;
use super::funcs::{bytes_to_human, process_args, search_dir};

pub struct RDU {
    config: Config,
}

impl RDU {
    pub fn new() -> RDU {
        RDU {
            config: Config {
                human: false,
                list: false,
                color: false,
            },
        }
    }

    pub fn start(&mut self) -> Result<(), Error> {
        let matches = process_args();

        self.update_config(&matches);

        let path = Path::new(matches.value_of("path").unwrap_or("."));

        let metadata = fs::metadata(path)?;

        let size: u64;

        if metadata.is_dir() {
            size = search_dir(path, self.config.list, self.config.human)?;
        } else {
            size = metadata.len();
        }

        if self.config.human {
            println!("{}", bytes_to_human(size));
        } else {
            println!("{}", size);
        }

        Ok(())
    }

    fn update_config(&mut self, args: &ArgMatches) {
        self.config.human = args.is_present("human");
        self.config.list = args.is_present("list");
        self.config.color = args.is_present("color");
    }
}
