use std::fs;
use std::io::Error;
use std::path::Path;
use clap::{Command, ArgMatches, Arg};

pub fn process_args() -> ArgMatches {
    Command::new("Rusty Disk Usage")
        .version("1.0")
        .author("DigitalCyan")
        .about("Get the size of a file or all the files within a folder.")
        .arg(
            Arg::new("path")
                .value_name("PATH")
                .help("The path to the file or a directory you wish to perform a size check on."),
        )
        .arg(
            Arg::new("list")
                .short('l')
                .long("list")
                .help("List every file"),
        )
        .arg(
            Arg::new("human")
                .short('h')
                .long("human")
                .help("Make the output human readable"),
        )
        .arg(
            Arg::new("color")
            .short('c')
            .long("color")
            .help("Print in color")
        )
        .get_matches()
}

pub fn bytes_to_human(size: u64) -> String {
    let level = size_level(size);
    match level {
        0 => {
            return format!("{} B", size);
        }

        1 => {
            return format!("{} kB", size / 1000);
        }

        2 => {
            return format!("{} MB", size / 1000u64.pow(2));
        }

        3 => {
            return format!("{} GB", size / 1000u64.pow(3));
        }

        _ => {
            return String::from("wtf");
        }
    }
}

pub fn search_dir(path: &Path, list: bool, human: bool) -> Result<u64, Error> {
    let mut sum: u64 = 0;

    for entry in fs::read_dir(path)? {
        if entry.is_err() {
            return Err(entry.unwrap_err());
        }

        let dir_entry = entry.unwrap();

        let metadata = fs::metadata(dir_entry.path())?;

        if metadata.is_dir() {
            sum += search_dir(&dir_entry.path(), list, human)?;
        } else {
            let size = metadata.len();
            sum += size;

            if list {
                if !human {
                    println!("{} {}", dir_entry.path().to_str().unwrap(), size);
                } else {
                    println!(
                        "{} {}",
                        dir_entry.path().to_str().unwrap(),
                        bytes_to_human(size)
                    );
                }
            }
        }
    }

    return Ok(sum);
}

fn digit_count(mut n: u64) -> u16 {
    let mut count = 0;

    while n != 0 {
        n = n / 10;
        count += 1;
    }

    return count;
}

fn size_level(size: u64) -> u16 {
    let mut level = (digit_count(size) / 3u16).clamp(0, 3);
    if level > 0 {
        level -= 1;
    }
    return level;
}
