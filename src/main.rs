mod lib;

use std::io::Error;
use lib::rdu::RDU;

fn main() -> Result<(), Error>{
    RDU::new().start()?;
    return Ok(());
}

