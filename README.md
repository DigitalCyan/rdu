<h1 align="center">Rusty Disk Usage</h1>

## About
Rusty Disk Usage is a program that writes the size of a file or a directory to stdout. If you target a file it will write it's size in bytes to stdout. Don't worry, you won't need a calculator, there's a flag to make the output more human readable. If you however target a directory, it will perform a recursive search of the targeted directory and sum up all the files it can find. There's also an option to print out every file individually.

---

## How to compile
1. Make sure you have `cargo` installed
2. Clone the repo
3. `cd` into the repo
4. Do: `cargo build --release`
5. Enjoy your binary located in `target/release/rdu`

---

## How to use
```sh
rdu [OPTIONS] [PATH]
```

### Examples:
These will print the size of a file or a directory in bytes
```
rdu Programs
```

```
rdu file.txt
```

If you wish to make it more readable pass the `--human` or `-h` flag like so:
```
rdu -h file.txt
```

---

## Current issues:
Does not work with absolute paths for some reason.

---

<p align="center">Made by DigitalCyan - Licensed under GPLv3</p>
